import { useEffect, useState } from 'react';
import classes from './App.module.css';
import WordGrid from './Components/WordGrid';
import _raw from './words/availableWordsES.txt'; //este archivo esta en mi ubicacion, contiene el texto "Hola Mundo!!..."
import Modal from './Components/UI/Modal';
import Keyboard from './Components/UI/Keyboard';
import Cookies from 'js-cookie';

function App() {
  const [timesWon, setTimesWon] = useState(0);
  const [currentTimesWonInRow, setCurrentTimesWonInRow] = useState(0);
  const [maxTimesWonInRow, setMaxTimesWonInRow] = useState(0);
  const [timesPlayed, setTimesPlayed] = useState(0);
  const [score, setScore] = useState(0);
  const [showStats, setShowStats] = useState(true);
  useEffect(() => {
    const storedTimesWon = Cookies.get('timesWon');
    if (storedTimesWon) {
      setTimesWon(storedTimesWon);
    }
    const storedCurrentTimesWonInRow = Cookies.get('currentTimesWonInRow');
    if (storedCurrentTimesWonInRow) {
      setCurrentTimesWonInRow(storedCurrentTimesWonInRow);
    }
    const storedMaxTimesWonInRow = Cookies.get('maxTimesWonInRow');
    if (storedMaxTimesWonInRow) {
      setMaxTimesWonInRow(storedMaxTimesWonInRow);
    }
    const storedTimesPlayed = Cookies.get('timesPlayed');
    if (storedTimesPlayed) {
      setTimesPlayed(storedTimesPlayed);
    }
    const storedScore = Cookies.get('score');
    if (storedScore) {
      setScore(storedScore);
    }
  }, []);
  const inputInit = [
    { value: [], active: true },
    { value: [], active: false },
    { value: [], active: false },
    { value: [], active: false },
    { value: [], active: false },
    { value: [], active: false }
  ]
  const [inputArray, setInputArray] = useState(inputInit)
  const [tries, setTries] = useState(1)
  const [word, setWord] = useState("")
  const [activeInput, setActiveInput] = useState()
  const [gameState, setGameState] = useState('playing')
  const [availableWords, setAvailableWords] = useState()
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalChild, setModalChild] = useState(false);
  const changeInputArray = (inputValue) => {
    let newInputArray = [...inputArray]
    newInputArray.forEach((input) => {
      if (input.active) {
        input.value[activeInput.attributes.index.value] = inputValue
      }
    })
    if (activeInput.nextElementSibling) {
      setActiveInput(activeInput.nextElementSibling)
    }
    setInputArray(newInputArray)
  }
  /* Llamado para traer las palabras */
  useEffect(() => {
    fetch(_raw)
      .then(r => r.text())
      .then(text => {
        setAvailableWords(text.replaceAll("\r\n", "").toUpperCase().split(','))
        setWord(() => text.split(',')[Math.floor(Math.random() * 34358)].toUpperCase().trim())
      })
  }, [])
  /* Fin del llamado para traer las palabras */


  /* Fin del Event Listener para el ingreso por teclado */

  useEffect(() => {
    let inputs = document.querySelectorAll('div.charInput:not([disabled])')
    setActiveInput(inputs[0])
  }, [tries])
  useEffect(() => {
    const handleKeyDown = (event) => {
      // Aquí puedes manejar la lógica del evento de teclado
      if ((event.keyCode >= 48 && event.keyCode <= 90) || (event.keyCode >= 96 && event.keyCode <= 111)) {
        if (/^[A-Za-zñÑ]+$/.test(event.key)) {
          changeInputArray(event.key.toUpperCase())
        }
      }
      if (event.key === "Enter") {
        handleSendClick()
      }
      if (event.key === "Backspace" || event.key === "Delete") {
        //changeActiveInput(" ")
      }
    };

    // Agrega el evento de escucha al montar el componente
    document.addEventListener('keydown', handleKeyDown);

    // Elimina el evento al desmontar el componente para evitar pérdidas de memoria
    return () => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [activeInput, inputArray]);
  useEffect(() => {
    if (word) {
      let newInputArray = [
        { value: [], active: true },
        { value: [], active: false },
        { value: [], active: false },
        { value: [], active: false },
        { value: [], active: false },
        { value: [], active: false }
      ]
      newInputArray.forEach((array, indexArray) => {
        word.split('').forEach((char, indexChar) => {
          newInputArray[indexArray].value[indexChar] = ""
        })
      })
      let inputs = document.querySelectorAll('div.charInput:not([disabled])')
      setActiveInput(inputs[0])
      setInputArray(newInputArray)
    }
  }, [word])
  const handleGameLost = () => {
    setCurrentTimesWonInRow(0);
    Cookies.set('currentTimesWonInRow', 0, { expires: 100 });
    let newTimesPlayed = timesPlayed;
    newTimesPlayed++
    setTimesPlayed(newTimesPlayed);
    Cookies.set('timesPlayed', newTimesPlayed, { expires: 100 });
    let newScore = score;
    newScore += 20
    setScore(newScore);
    Cookies.set('score', newScore, { expires: 100 });
    setShowStats(true)
  }
  const handleWin = () => {
    let newTimesWon = timesWon;
    newTimesWon++
    setTimesWon(newTimesWon);
    Cookies.set('timesWon', newTimesWon, { expires: 100 });
    let newCurrentTimesWonInRow = currentTimesWonInRow;
    newCurrentTimesWonInRow++
    setCurrentTimesWonInRow(newCurrentTimesWonInRow);
    Cookies.set('currentTimesWonInRow', newCurrentTimesWonInRow, { expires: 100 });
    let newMaxTimesWonInRow = maxTimesWonInRow;
    if (newCurrentTimesWonInRow > newMaxTimesWonInRow) {
      newMaxTimesWonInRow++
      setMaxTimesWonInRow(newMaxTimesWonInRow);
      Cookies.set('maxTimesWonInRow', newMaxTimesWonInRow, { expires: 100 });
    }
    let newTimesPlayed = timesPlayed;
    newTimesPlayed++
    setTimesPlayed(newTimesPlayed);
    Cookies.set('timesPlayed', newTimesPlayed, { expires: 100 });
    let newScore = score;
    newScore = parseInt(newScore) + parseInt(Math.floor(700 / tries))
    setScore(newScore);
    Cookies.set('score', newScore, { expires: 100 });
    setShowStats(true)
  }

  const handleSendClick = () => {
    let inputs = document.querySelectorAll('div.charInput:not([disabled])')
    let inputWord = ""
    inputs.forEach((element) => inputWord += element.firstChild.nodeValue)
    if (inputWord === word) {
      handleWin()
      setInputArray((prevArray) => {
        const newArray = [...prevArray];
        newArray.forEach((input, index) => {
          if (input.active) {
            input.value = inputWord.split('')
            input.active = false
          }
        })
        return newArray;
      })
      setGameState("won")
      return
    }
    if (inputWord.length !== word.length) {
      setInputArray((prevArray) => {
        const newArray = [...prevArray];
        newArray.forEach((input) => {
          if (input.active) {
            input.value = inputWord.split('')
          }
        })
        return newArray;
      })
      setModalChild('Incorrect Length')
      setIsModalOpen(true)
      return
    }
    if (!availableWords.includes(inputWord)) {
      setInputArray((prevArray) => {
        const newArray = [...prevArray];
        newArray.forEach((input) => {
          if (input.active) {
            input.value = inputWord.split('')
          }
        })
        return newArray;
      })
      setModalChild('That word is not valid')
      setIsModalOpen(true)
      return
    }
    if (tries === 6) {
      handleGameLost()
      setGameState('lost')
    }
    setInputArray((prevArray) => {
      const newArray = [...prevArray];
      let newActiveInput = 0
      newArray.forEach((input, index) => {
        if (input.active) {
          input.value = inputWord.split('')
          input.active = false
          newActiveInput = index + 1
        }
      })
      if (newArray[newActiveInput]) {
        newArray[newActiveInput].active = true
      }
      return newArray;
    })
    setTries((prevValue) => {
      let newValue = prevValue
      newValue++
      return newValue
    })
  }
  const changeActiveInput = (target) => {
    setActiveInput(target)
  }
  const closeModal = () => {
    setIsModalOpen(false);
  };
  const restartGame = () => {
    setInputArray(inputInit)
    setTries(1)
    setWord(() => availableWords[Math.floor(Math.random() * 34358)].toUpperCase().trim())
    setGameState('playing')
    setShowStats(false)
  }

  const toggleShowStat = () => {
    let newState = showStats
    newState = !newState
    setShowStats(newState)
  }
  return (
    <div className={classes.App}>
      <Modal isOpen={isModalOpen} onClose={closeModal} >
        {modalChild}
      </Modal>
      <h1>Discover the word</h1>
      {
        (word !== "") &&
        inputArray.map((element) =>
          <WordGrid activeInput={activeInput} changeActiveInput={changeActiveInput} active={element.active} key={Math.random()} inputWord={element.value} secretWord={word}></WordGrid>)
      }
      {gameState === "playing" && <input className={classes.sendButton} type="button" value="Send" onClick={handleSendClick} />}
      {gameState === "playing" && <Keyboard changeInputArray={changeInputArray} secretWord={word} inputArray={inputArray}></Keyboard>}
      <div>
        <input type="button" value="Show stats" onClick={() => toggleShowStat()} />
      </div>
      {gameState === "won" && <div><h1>You WON! the word was: {word}</h1><button className={classes.newGameButton} onClick={restartGame} >Play Again</button></div>}
      {gameState === "lost" && <div><h1>You LOST, the word was: {word}</h1><button className={classes.newGameButton} onClick={restartGame} >Try Another</button></div>}
      {showStats && <div>
        <h2>Times Won: {timesWon}</h2>
        <h2>Max Wins in a row: {maxTimesWonInRow}</h2>
        <h2>Current Wins in a row: {currentTimesWonInRow}</h2>
        <h2>Times Played: {timesPlayed}</h2>
        <h2>Victory percentage: {timesPlayed > 0 ? Math.floor(timesWon * 100 / timesPlayed) : 0} %</h2>
        <h2>Score: {score}</h2>
      </div>}
    </div>
  );
}

export default App;
