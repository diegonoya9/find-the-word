import classes from './CharInput.module.css'
import { useState } from 'react'

const CharInput = ({ index, active, value, color, changeActiveInput, activeInput }) => {
  const [inputValue, setInputValue] = useState(value)
  const moveToNext = (input) => {
    // Set the new value in UpperCase
    setInputValue(input.target.value.toUpperCase())
    // Obtain next input
    const nextInput = input.target.nextElementSibling;
    // If there is a next input, then focus on it
    if (nextInput !== null) {
      nextInput.focus()
    }
  }
  let selected = false
  if (activeInput) {
    if (index == activeInput.attributes.index.value && active) {
      selected = true
    }
  }
  const handleClick = (event) => {
    changeActiveInput(event.target)
  }
  return (
    <div style={{ "backgroundColor": color }} index={index} onClick={(event) => { if (active) { handleClick(event) } else { console.log('inactive') } }} className={`${classes.charInput} charInput ${selected && classes.selected}`} disabled={!active} >
      {inputValue ? inputValue : ''}
    </div >
  )
}

export default CharInput;