import classes from './Keyboard.module.css'
import { useEffect, useState, useMemo } from 'react'

const Keyboard = ({ secretWord, inputArray, changeInputArray }) => {
    const keyboardInit = useMemo(() => {
        return [
            { value: "A", class: "notUsed" },
            { value: "B", class: "notUsed" },
            { value: "C", class: "notUsed" },
            { value: "D", class: "notUsed" },
            { value: "E", class: "notUsed" },
            { value: "F", class: "notUsed" },
            { value: "G", class: "notUsed" },
            { value: "H", class: "notUsed" },
            { value: "I", class: "notUsed" },
            { value: "J", class: "notUsed" },
            { value: "K", class: "notUsed" },
            { value: "L", class: "notUsed" },
            { value: "M", class: "notUsed" },
            { value: "N", class: "notUsed" },
            { value: "Ñ", class: "notUsed" },
            { value: "O", class: "notUsed" },
            { value: "P", class: "notUsed" },
            { value: "Q", class: "notUsed" },
            { value: "R", class: "notUsed" },
            { value: "S", class: "notUsed" },
            { value: "T", class: "notUsed" },
            { value: "U", class: "notUsed" },
            { value: "V", class: "notUsed" },
            { value: "W", class: "notUsed" },
            { value: "X", class: "notUsed" },
            { value: "Y", class: "notUsed" },
            { value: "Z", class: "notUsed" }
        ]
    }, [])
    const [keyboard, setKeyboard] = useState(keyboardInit)
    useEffect(() => {
        let ocurrences = {}
        let secretWordArray = secretWord.split('')
        let newKeyboard = [...keyboard]
        if (inputArray) {
            //Set the ocurrences counter
            // console.log(secretWord)
            secretWord.split('').forEach((char) => {
                if (!ocurrences[char]) {
                    ocurrences[char] = 1
                } else {
                    ocurrences[char]++
                }
            })
            inputArray.forEach((input) => {
                if (input.value && !input.active) {
                    /* Painting the green keys first*/
                    input.value.forEach((char, index) => {
                        if (secretWordArray.indexOf(char) >= 0) {
                            secretWordArray.forEach((secretChar, secretIndex) => {
                                if (secretChar === char && secretIndex === index) {
                                    newKeyboard.forEach((key) => {
                                        if (key.value === char) {
                                            key.class = "correct"
                                            //console.log('verde')
                                        }
                                        return key
                                    })
                                    ocurrences[char]--
                                }
                            })
                        } else {
                            newKeyboard.forEach(key => {
                                if (key.value === char) {
                                    key.class = "incorrect"
                                }
                            })
                        }
                    })
                    //Search for correct char incorrect position 
                    input.value.forEach((char, index) => {
                        if (secretWordArray.indexOf(char) >= 0 && ocurrences[char] > 0) {
                            secretWordArray.forEach((secretChar, secretIndex) => {
                                if (secretChar === char && secretIndex !== index) {
                                    newKeyboard.forEach((key) => {
                                        if (key.value === char && key.class !== "correct") {
                                            key.class = "halfCorrect"
                                            //console.log('naranja')
                                        }
                                        return key
                                    })
                                    ocurrences[char]--
                                }
                            })
                        }
                    })

                }
            })
        }
        setKeyboard(() => { return newKeyboard })
    }, [inputArray, secretWord, keyboardInit])
    return (
        <div className={classes.keyboard}>
            {keyboard && keyboard.map((key) => {
                return <input key={key.value} type="button" value={key.value} onClick={() => changeInputArray(key.value)} className={classes[key.class]} />
            })}
        </div>
    )
}

export default Keyboard