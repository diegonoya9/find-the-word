import classes from './Modal.module.css'

const Modal = ({ isOpen, onClose, children }) => {
    if (!isOpen) {
        return null;
    }

    return (
        <div className={classes.modalOverlay} onClick={onClose}>
            <div className={classes.modalContent} onClick={(e) => e.stopPropagation()}>
                <span className={classes.closeButton} onClick={onClose}>
                    &times;
                </span>
                <h1>{children}</h1>
            </div>
        </div>
    );
};

export default Modal