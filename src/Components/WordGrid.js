import { useEffect, useState, useMemo } from 'react'
import CharInput from './CharInput'
import classes from './WordGrid.module.css'

const WordGrid = ({ secretWord, inputWord, active, changeActiveInput, activeInput }) => {
    const secretWordArray = useMemo(() => { return secretWord.split('') }, [secretWord])
    const [colorArray, setColorArray] = useState([])
    useEffect(() => {
        let ocurrences = {}
        let newColorArray = []

        if (inputWord[0] && !active) {
            //Set the ocurrences counter
            secretWord.split('').forEach((char) => {
                if (!ocurrences[char]) {
                    ocurrences[char] = 1
                } else {
                    ocurrences[char]++
                }
            })
            //Search for correct char in correct position first 
            inputWord.forEach((char, index) => {
                if (secretWordArray.indexOf(char) >= 0) {
                    secretWordArray.forEach((secretChar, secretIndex) => {
                        if (secretChar === char && secretIndex === index) {
                            newColorArray[index] = 'lightgreen'
                            ocurrences[char]--
                        }
                    })
                } else {
                    newColorArray[index] = 'grey'
                }
            })
            //console.log(newColorArray)
            //Search for correct char incorrect position 
            inputWord.forEach((char, index) => {
                if (secretWordArray.indexOf(char) >= 0 && ocurrences[char] > 0) {
                    secretWordArray.forEach((secretChar, secretIndex) => {
                        if (secretChar === char && secretIndex !== index && newColorArray[index] !== "lightgreen") {
                            newColorArray[index] = 'orange'
                            ocurrences[char]--
                        }
                    })
                }
            })
            setColorArray(newColorArray)
        }
        if ((inputWord[0] && active) || (!inputWord[0])) {
            secretWordArray.forEach((secretChar, secretIndex) => {
                newColorArray[secretIndex] = 'white'
            })
            setColorArray(newColorArray)
        }
    }, [inputWord, secretWordArray, secretWord, active])

    return (
        <div className={classes.wordGrid} >
            {secretWordArray && secretWordArray.map((value, index) => <CharInput activeInput={activeInput} index={index} changeActiveInput={changeActiveInput} color={colorArray[index]} value={inputWord ? inputWord[index] : ''} key={Math.random()} active={active}></CharInput>)}
        </div>
    )
}

export default WordGrid